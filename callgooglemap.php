<?php

/*
 * Callgooglemap.php
 * This programe can get xml data from http://maps.googleapis.com/maps/api/directions/xml?origin=khon kaen&destination=Bangkok&sensor=false
 * and display content in instruction element as xml format.
 * 
 * @author Chaiyaphum Siripanpornchana
 * id : 533040441-5
 */

header("Content-type: text/xml; charset=utf-8");

// $dom_read for read xml content
$dom_read = new DOMDocument;
// $dom_read for write xml content
$dom_write = new DomDocument('1.0','UTF-8');

$dom_read->load('http://maps.googleapis.com/maps/api/directions/xml?origin=khon%20kaen&destination=Bangkok&sensor=false');

$root = $dom_read->documentElement;
$elms = $root->getElementsByTagName("step");

// Create instructions as root element
$root_Writer = $dom_write->appendChild($dom_write->createElement('instructions'));

foreach ($elms AS $item) {
	// Read content from element html_instructions
	$instructionsContent = $item->getElementsByTagName('html_instructions')->item(0)->nodeValue;

	// Create instruction for subelement of instructions
	$subItem = $root_Writer->appendChild($dom_write->createElement('instruction'));
	$subItem->appendChild( $dom_write->createTextNode($instructionsContent));
}

$dom_write->formatOutput = true;
$showData = $dom_write->saveXML();
echo $showData;

?>