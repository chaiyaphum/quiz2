<?php

/*
 * Callkkmarathon.php
 * This programe can get json data from https://rdttc.kku.ac.th/itsupport/kkmarathon/search.php?q=henrik&output=json
 * and display content in name element as json format.
 * 
 * @author Chaiyaphum Siripanpornchana
 * id : 533040441-5
 */

// Can get other name by call Callkkmarathon.php?name=YOUR_NAME, Default name as "henrik"
$qInput = (isset($_GET['name']))? $_GET['name'] : "henrik";
$url = "https://rdttc.kku.ac.th/itsupport/kkmarathon/search.php?q=".$qInput."&output=json";

// Access ssl protected sites
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($ch);
curl_close($ch);

// Decode json data
$jsonDecode = json_decode($response, true);
$arrayNameContent = array();

foreach($jsonDecode['result'] as $resultContent) {
	// Add content of element name to array
	array_push($arrayNameContent,$resultContent['name']);
}

$arrayJsonContent = array('names' => $arrayNameContent);

// Print json data and encode with utf-8
echo utf8_encode(json_encode($arrayJsonContent));

?>